/*
 * ecat4xnat: org.nrg.ecat.Header
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat;

import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;

public interface Header {
	public static enum Type { MAIN, IMAGE };
	
	public Type getType();
	
	public Object set(Variable var, Object value);
	
	public void read(ByteBuffer b, Collection<Variable> vars);
	
	public void add(Map<Variable,?> vars);
	
	public Object get(Variable var);
	
	public SortedMap<Variable,?> getValues();
	
	/**
	 * Generates a simple XML representation of the header contents
	 * @param out
	 */
	public void emitXML(PrintStream out);
}
