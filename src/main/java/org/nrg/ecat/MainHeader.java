/*
 * ecat4xnat: org.nrg.ecat.MainHeader
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat;


public final class MainHeader extends AbstractHeader implements Header {
	public MainHeader() { super(Header.Type.MAIN); }
}
