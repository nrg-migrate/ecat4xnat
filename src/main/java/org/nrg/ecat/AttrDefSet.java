/*
 * ecat4xnat: org.nrg.ecat.AttrDefSet
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.ecat;

import org.nrg.attr.MutableAttrDefs;

public final class AttrDefSet extends MutableAttrDefs<Variable> {}
